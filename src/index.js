const express = require("express");
const app = express();

app.get('/', (req, res) => res.send('hello beauty'));
app.get('/users', (req, res) => {
    res.json({"name": "Mario", "age": 42})
})

app.listen(3000, () => {
    console.log("Hello beuty at port 3000");
});