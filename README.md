<h3>Ferramenta logrotate</h3>
<ul>
<li>source: https://www.dicas-l.com.br/arquivo/utilizando_logrotate.php, https://linux.die.net/man/8/logrotate</li>
<li>install: sudo apt install logrotate</li>
<li>global config: /etc/logrotate.conf</li>
<li>especific config for apps (apache, docker): /etc/logrotate.d/app-folder</li>
</ul>

<h3>Docker and logrotate</h3>
<ul>
<li>source: https://sandro-keil.de/blog/logrotate-for-docker-container/, https://blog.linoproject.net/tech-tip-docker-logs/, https://sandro-keil.de/blog/docker-with-overlayfs-on-ubuntu/</li>
<li>docker container log files are saved in /var/lib/docker/containers/[CONTAINDER ID]/[CONTAINER ID]-json.log</li>
<li>enable logrotate for docker: create a logrotate config for docker containers -> /etc/logrotate.d/docker-container</li>
<li>config for all containers (docker-container file content) file example: 
    /var/lib/docker/containers/*/*.log {
        rotate 7
        daily
        compress
        missingok
        delaycompress
        copytruncate
    }
</li>
<li>config for especific container (docker-container file content) file example: 
    /var/lib/docker/containers/container_id/file-json.log {
        rotate 7
        daily
        compress
        missingok
        delaycompress
        copytruncate
    }
</li>
</ul>