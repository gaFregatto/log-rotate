FROM mhart/alpine-node:12

# Diretório de trabalho
WORKDIR /usr/app

# Copia arquivos package para /usr/app
COPY package*.json ./
RUN npm install

# Copia restante dos arquivos, ignora arquivos do .dockerignore
COPY . . 

# Porta exposta do container
EXPOSE 3000

# Única por Dockerfile, indica qual comando o servidor precisa rodar para startar
CMD ["npm", "start"]

